#!/usr/bin/env python3
# Copyright (C) 2020 Denis 'GNUtoo' Carikli <GNUtoo@cyberdimension.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from bs4 import BeautifulSoup, element

index = open('index.html', 'r')

soup = BeautifulSoup(index, 'html.parser')

# Change the <title>
soup.title.string = "Free software support status for FPGAs"

# Remove all the JavaScript
for tag in soup.find_all('script'):
    tag.extract()

for link in soup.head.find_all('link'):
    # Remove the external CSS
    if not link['href'].startswith('assets/'):
        link.extract()
    # Remove github related CSS
    if link['href'] == 'assets/css/github-activity.css':
        link.extract()
    # There is a <-- Glide.js --> comment on top
    # So we can safely remove them
    if link['href'].startswith('assets/css/glide.'):
        link.extract()

for line in soup.head:
    # Remove some comments that are not relevant anymore
    # because the elements below have been removed already
    if type(line) == type(element.Comment('')):
        if line.strip() == 'Glide.js':
            line.extract()
        elif line.strip() == 'glightbox':
            line.extract()
        elif line.strip() == 'Github Feed':
            line.extract()

# Remove gsoc banner
for line in soup.body:
    # Sometimes the gsoc banner is commented
    if type(line) == type(element.Comment('')):
        try:
            comment = BeautifulSoup(line, 'html.parser')
            if 'gsoc-banner' in comment.div.get('class', {}):
                line.extract()
        except:
            pass

for div in soup.find_all('div'):
    # Remove the remaining violet footer
    if 'downloads__wrapper' in div.get('class', {}):
        div.extract()
    # Sometimes a gsoc-banner probably appears, remove that
    elif 'gsoc-banner' in div.get('class', {}):
        div.extract()

# Remove the navbar as it's not relevant to the status
for tag in soup.find_all('nav'):
    tag.extract()

# Also remove the footer
for tag in soup.find_all('footer'):
    tag.extract()

for section in soup.find_all('section'):
    if 'diagrams' in section['class']:
        for elm in section.findAll():
            # Remove "How it works"
            if 'row__header' in elm.get('class', {}):
                elm.extract()
            # Remove the introduction
            elif 'diagrams__paragraph_intro' in elm.get('class', {}):
                elm.extract()
            # Remove the EDA ecosystem explanation
            elif 'eda-ecosystem' in elm.get('id', {}):
                elm.extract()
            # And the associated diagrams
            elif 'structure' in elm.get('id', {}):
                elm.extract()
            # Add some text at the beginning:
            elif 'diagram__header' in elm.get('class', {}):
                if elm.string.strip() == "Current status":
                    elm.string = "Current status (based on symbiflow.github.io):"
                    
    elif 'boards' in section['class']:
        for elm in section.findAll():
            if 'boards__showcase-d-none' in elm.get('class', {}):
                elm['class'].remove('boards__showcase-d-none')
            if 'boards__showcase-hidden' in elm.get('class', {}):
                elm['class'].remove('boards__showcase-hidden')
            if 'boards__showcase-delayed' in elm.get('class', {}):
                elm['class'].remove('boards__showcase-delayed')
    else:
        section.extract()

clean = open('clean.html', 'w')
clean.write(soup.prettify())
clean.close()
